//
//  TalkTalkTests.swift
//  TalkTalkTests
//
//  Created by Сергей Петрук on 28.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import XCTest
@testable import TalkTalk

class GoServiceTests: XCTestCase {
    
    let goService = GoService(userId: "bder26us2h0afhc274n0")
    
    func testRegister() {
        let expectation = XCTestExpectation(description: "")
        
        goService.register(param: RegisterInputParam(facebookId: "100003333214427", displayName: "Sergey Petruk")) { (result) in
            switch result {
            case .result(let res):
                XCTAssertNotNil(res.userId)
                expectation.fulfill()
            case .fail( _):
                XCTFail()
            }
        }
        wait(for: [expectation], timeout: 3.0)
    }
    
    func testPosts() {
        let expectation = XCTestExpectation(description: "")
        
        goService.posts(param: PostsInputParam(latitude: 51.5033640, longitude: -0.1276250, radius: 50)) { (result) in
            switch result {
            case .result( _):
                expectation.fulfill()
            case .fail( _):
                XCTFail()
            }
        }
        wait(for: [expectation], timeout: 3.0)
    }
    
    func testComments() {
        let expectation = XCTestExpectation(description: "")
        
        goService.comments(param: CommentsInputParam(postId: "post_id_1")) { (result) in
            switch result {
            case .result( _):
                expectation.fulfill()
            case .fail( _):
                XCTFail()
            }
        }
        wait(for: [expectation], timeout: 3.0)
    }
    
    func testAddPost() {
        let expectation = XCTestExpectation(description: "")
        
        goService.addPost(param: AddPostInputParam(latitude: 51.5033640, longitude: -0.1276250, message: "Test message", typeId: TalkType.random()), completion: { (result) in
            switch result {
            case .success:
                expectation.fulfill()
            case .fail( _):
                XCTFail()
            }
        })
        wait(for: [expectation], timeout: 3.0)
    }
    
    func testAddComment() {
        let expectation = XCTestExpectation(description: "")
        
        goService.addComment(param: AddCommentInputParam(postId: "post_id_1", message: "Test message")) { (result) in
            switch result {
            case .success:
                expectation.fulfill()
            case .fail( _):
                XCTFail()
            }
        }
        wait(for: [expectation], timeout: 3.0)
    }
    
}
