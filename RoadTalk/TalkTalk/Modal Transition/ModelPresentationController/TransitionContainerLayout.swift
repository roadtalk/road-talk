//
//  TransitionContainerLayout.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 28.07.2018.
//

import UIKit

// Layout
extension TransitionContainerViewController {
    func layoutTopImageView(_ topImageView: UIImageView, configuration: TopImageConfiguration) {
        topImageView.translatesAutoresizingMaskIntoConstraints = false
        topImageView.pin(toSuperview: .top, offset: configuration.topOffset)
        topImageView.setDimension(.width, equalTo: configuration.width)
        topImageView.setDimension(.height, equalTo: configuration.height)
        
        NSLayoutConstraint(item: topImageView,
                           attribute: .centerX,
                           relatedBy: .equal,
                           toItem: topImageView.superview,
                           attribute: .centerX,
                           multiplier: 1,
                           constant: 0).isActive = true
    }
    
    func layoutContainerView(_ containerView: UIView, topOffset: CGFloat) {
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        containerView.pin(toSuperview: .top, offset: topOffset)
        containerView.pin(toSuperview: .left)
        containerView.pin(toSuperview: .right)
        containerView.pin(toSuperview: .bottom)
    }
    
    func layoutTopVisualEffectView(_ topVisualEffect: UIVisualEffectView, targetControllerView: UIView) {
        
        topVisualEffect.translatesAutoresizingMaskIntoConstraints = false
        
        topVisualEffect.pin(toSuperview: .left)
        topVisualEffect.pin(toSuperview: .top)
        topVisualEffect.pin(toSuperview: .right)
        NSLayoutConstraint(item: topVisualEffect,
                           attribute: .bottom,
                           relatedBy: .equal,
                           toItem: targetControllerView,
                           attribute: .top,
                           multiplier: 1,
                           constant: 0).isActive = true
    }
    
    func layoutTargetControllerView(_ targetView: UIView, topOffset: CGFloat) {
        targetView.translatesAutoresizingMaskIntoConstraints = false
        
        targetView.pin(toSuperview: .top, offset: topOffset)
        targetView.pin(toSuperview: .left)
        targetView.pin(toSuperview: .bottom)
        targetView.pin(toSuperview: .right)
    }
}
