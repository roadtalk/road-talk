//
//  ModalCustomSegue.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 28.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import UIKit

public class ModalCustomSegue: UIStoryboardSegue {
    override public func perform() {
        let transitionHelp = TransitionContainerViewController(targetViewController: destination)
        source.present(transitionHelp, animated: true, completion: nil)
    }
}
