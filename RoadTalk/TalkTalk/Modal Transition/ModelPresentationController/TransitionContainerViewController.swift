//
//  ModalPresentationContainer.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 28.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import UIKit

public struct TransitionConfiguration {
    
    public var isInteractive: Bool = true // defines is transition interactive
    public var closeOnTouchOnTop: Bool = true // close controller if touch on dark area
    public var topOffset: CGFloat = 10 // offset from status bar
    public var gapHeight: CGFloat = 30 // distance between controller view top and black area
    public var topCornersRadius: CGFloat = 10
    public var navigationBarColor: UIColor? = .white //use nil for regular navigation controller
    public var prefferedStatusBarStyle: UIStatusBarStyle = .lightContent
    public var topImageConfiguration: TopImageConfiguration? = TopImageConfiguration.default
    public var presentAnimationConfiguration: PresentAnimationConfiguration = PresentAnimationConfiguration.default
    
    public static var `default`: TransitionConfiguration {
        return TransitionConfiguration()
    }
}

public struct TopImageConfiguration {
    public var width: CGFloat = 36
    public var height: CGFloat = 12
    public var topOffset: CGFloat = 12
    public var image: UIImage?
    public var dismissOnClick: Bool = true // dismiss view controller on touch
    
    init() {
        image = UIImage(named: "ModalTransitionArrowDown", in: Bundle.currentBundle(), compatibleWith: nil)
    }
    
    public static var `default`: TopImageConfiguration {
        return TopImageConfiguration()
    }
}

public class TransitionContainerViewController: UIViewController {
    
    private let transactionDelegate: PresentingTransitionDelegate // swiftlint:disable:this weak_delegate
    
    private let targetViewController: UIViewController
    private let configuration: TransitionConfiguration
    private let containerView: UIView
    private let topVisualEffect: UIVisualEffectView?
    private var topImageView: UIImageView?
    
    public init(targetViewController: UIViewController, configuration: TransitionConfiguration = TransitionConfiguration.default) {
        self.transactionDelegate = PresentingTransitionDelegate(configuration: configuration.presentAnimationConfiguration)
        self.targetViewController = targetViewController
        self.configuration = configuration
        self.containerView = UIView()
        self.containerView.translatesAutoresizingMaskIntoConstraints = false
        self.topImageView = UIImageView()
        
        // if target controller is UINavigationController setup is slight different
        let navigationController: UINavigationController? = targetViewController as? UINavigationController
        
        let style: UIBlurEffectStyle = configuration.navigationBarColor == nil ? .extraLight : .light
        if configuration.gapHeight > 0, navigationController?.isNavigationBarHidden == false {
            self.topVisualEffect = UIVisualEffectView(effect: UIBlurEffect(style: style))
            self.topVisualEffect?.backgroundColor = configuration.navigationBarColor
        } else {
            self.topVisualEffect = nil
        }
        super.init(nibName: nil, bundle: nil)
        
        modalPresentationStyle = .custom
        modalPresentationCapturesStatusBarAppearance = true
        transitioningDelegate = transactionDelegate
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        setupContainerView()
        setupTargetViewController()
        setupTopView()
        setupTopImageView()
        setupCloseGestureRecognizer()
    }
    
    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        roundViewTopCorners()
        
        // fix status bar skipping on ios 8-9
        if #available(iOS 10, *) {
            return
        }
        let navigationController: UINavigationController? = targetViewController as? UINavigationController
        navigationController?.navigationBar.frame.origin.y = 0
    }
    
    private func roundViewTopCorners() {
        let maskLayer = CAShapeLayer()
        maskLayer.frame = containerView.bounds
        maskLayer.path = UIBezierPath(roundedRect: containerView.bounds,
                                      byRoundingCorners: [.topLeft, .topRight],
                                      cornerRadii: CGSize(width: configuration.topCornersRadius,
                                                          height: configuration.topCornersRadius)).cgPath
        containerView.layer.mask = maskLayer
    }
    
    private func setupTopView() {
        guard let topVisualEffect = topVisualEffect else {
            return
        }
        containerView.addSubview(topVisualEffect)
        layoutTopVisualEffectView(topVisualEffect, targetControllerView: targetViewController.view)
    }
    
    private func setupContainerView() {
        view.addSubview(containerView)
        layoutContainerView(containerView, topOffset: topOffset)
        containerView.backgroundColor = .white
    }
    
    private func setupTargetViewController() {
        addChildViewController(targetViewController)
        targetViewController.willMove(toParentViewController: self)
        containerView.addSubview(targetViewController.view)
        layoutTargetControllerView(targetViewController.view, topOffset: configuration.gapHeight)
        targetViewController.didMove(toParentViewController: self)
    }
    
    private func setupTopImageView() {
        guard let topImageConfiguration = configuration.topImageConfiguration else {
            return
        }
        let topImageView = UIImageView()
        topImageView.contentMode = .scaleAspectFit
        topImageView.isUserInteractionEnabled = true
        topImageView.image = topImageConfiguration.image
        containerView.addSubview(topImageView)
        layoutTopImageView(topImageView, configuration: topImageConfiguration)
        self.topImageView = topImageView
        guard topImageConfiguration.dismissOnClick else {
            return
        }
        let dismissGesture = UITapGestureRecognizer(target: self, action: #selector(TransitionContainerViewController.dismissAction))
        topImageView.addGestureRecognizer(dismissGesture)
    }
    
    private func setupCloseGestureRecognizer() {
        guard configuration.closeOnTouchOnTop else {
            return
        }
        let closeGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TransitionContainerViewController.dismissAction))
        closeGestureRecognizer.delegate = self
        view.addGestureRecognizer(closeGestureRecognizer)
    }
    
    @objc private func dismissAction() {
        dismiss(animated: true, completion: nil)
    }
    
    private var topOffset: CGFloat {
        return configuration.topOffset + UIApplication.shared.statusBarFrame.height
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return configuration.prefferedStatusBarStyle
    }
}

extension TransitionContainerViewController: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == view
    }
}
