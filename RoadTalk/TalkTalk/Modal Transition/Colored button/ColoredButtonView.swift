//
//  ColoredButtonView.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 28.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation
import UIKit

class ColoredButtonView:UIView {

    public var selected:Bool = false {
        didSet {
            for imageView in subviews {
                if imageView.tag == -1 {
                    imageView.isHidden = !selected
                }
            }
        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        for imageView in subviews {
            if imageView.tag == -1 {
                imageView.isHidden = false
            }
        }
        
    }

    
}
