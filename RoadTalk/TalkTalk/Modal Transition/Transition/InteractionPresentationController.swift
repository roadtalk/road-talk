//
//  InteractionPresentationController.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 28.07.2018.
//

import UIKit

private struct Constants {
    static let minimumProgressThreshold: CGFloat  = 0.1
    static let centerProgressThreshold: CGFloat   = 0.4
    static let maximumProgressThreshold: CGFloat  = 0.7
    static let transitionSlowCoefficient: CGFloat = 0.9
    static let minimumVerticalValocity: CGFloat   = 500
    static let fastAnimationSpeed: TimeInterval = 0.25
}

public struct PresentAnimationConfiguration {
    
    public var shouldScale: Bool = true
    public var alpha: CGFloat = 0.85
    public var cornerRadius: CGFloat = 10
    public var duration: TimeInterval = 0.4
    
    public static var `default`: PresentAnimationConfiguration {
        return PresentAnimationConfiguration()
    }
}

class InteractionPresentationController: UIPresentationController {
    
    private var scrollViewHandler: ScrollViewHandler?
    private let panGesture: UIPanGestureRecognizer = UIPanGestureRecognizer()
    private var snapshot: UIView?
    private var backView: UIView?
    private lazy var snapshotDefaultScale: CGFloat = self.calculateSnapshotScale()
    private lazy var snapshotDefaultTransform: CGAffineTransform = CGAffineTransform(scaleX: snapshotDefaultScale, y: snapshotDefaultScale)
    private let configuration: PresentAnimationConfiguration
    private(set) var lastProgress: CGFloat?
    
    
    // MARK:  computed properties
    
    var timeToClose: TimeInterval {
        guard let lastProgress = lastProgress else { return configuration.duration }
        return Double(1 - lastProgress) * configuration.duration
    }
    var shouldScale: Bool {
        return configuration.shouldScale
    }
    private var rootView: UIView? {
        return containerView
    }
    private var rootViewHeight: CGFloat {
        return (rootView?.frame.height ?? UIScreen.main.bounds.height) - UIApplication.shared.statusBarFrame.height
    }
    private var isDismissEnabled: Bool {
        return scrollViewHandler?.isDismissEnabled ?? true
    }
    private var startLocation: CGFloat {
        return scrollViewHandler?.startLocation ?? 0
    }
    
    // MARK: lifecycle
    
    init(presentedViewController: UIViewController,
                     presenting presentingViewController: UIViewController?,
                     configuration: PresentAnimationConfiguration = PresentAnimationConfiguration.default) {
        self.configuration = configuration
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(InteractionPresentationController.updateStatusBar),
                                               name: .UIApplicationWillChangeStatusBarFrame,
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func presentationTransitionWillBegin() {
        guard let container = containerView,
              let snapshot = presentingViewController.view.snapshotView(afterScreenUpdates: true) else {
            return
        }
        
        setupPanGesture(withRootView: rootView)
        self.snapshot = snapshot
        snapshot.layer.masksToBounds = true
        let backView = UIView(frame: snapshot.frame)
        backView.backgroundColor = .black
        
        container.addSubview(backView)
        container.addSubview(snapshot)
        
        if shouldScale {
            snapshot.addCornerRadiusAnimation(from: 0,
                                              to: configuration.cornerRadius,
                                              duration: configuration.duration)
        }
        
        let coordinator = presentingViewController.transitionCoordinator
        
        coordinator?.animate(alongsideTransition: { context in
            snapshot.alpha = self.configuration.alpha
            snapshot.transform = self.snapshotDefaultTransform
        })
    }
    
    override func presentationTransitionDidEnd(_ completed: Bool) {
        guard completed else {
            return
        }
        guard let container = containerView,
              let newSnapshot = presentingViewController.view.snapshotView(afterScreenUpdates: true),
              let currentSnapshot = snapshot,
              let presentedView = presentedView else {
            return
        }
        newSnapshot.alpha = currentSnapshot.alpha
        newSnapshot.transform = currentSnapshot.transform
        newSnapshot.layer.cornerRadius = currentSnapshot.layer.cornerRadius
        newSnapshot.layer.masksToBounds = true
        
        container.insertSubview(newSnapshot, belowSubview: presentedView)
        currentSnapshot.removeFromSuperview()
        self.snapshot = newSnapshot
    }
    
    override func dismissalTransitionWillBegin() {
        guard let snapshot = self.snapshot else { return }
        let coordinator = presentingViewController.transitionCoordinator
        
        if shouldScale {
            snapshot.addCornerRadiusAnimation(from: configuration.cornerRadius,
                                              to: 0,
                                              duration: timeToClose)
        }
        coordinator?.animate(alongsideTransition: { context in
            snapshot.alpha = 1
            snapshot.transform = CGAffineTransform.identity
        })
    }
    
    // MARK: gesture recognizer
    
    private func setupPanGesture(withRootView rootView: UIView?) {
        guard let rootView = rootView else {
            return
        }
        panGesture.maximumNumberOfTouches = 1
        panGesture.cancelsTouchesInView = false
        panGesture.delegate = self
        panGesture.addTarget(self, action: #selector(InteractionPresentationController.handleGesture(_:)))
        
        rootView.addGestureRecognizer(panGesture)
    }
    
    @objc private func handleGesture(_ gestureRecognizer: UIPanGestureRecognizer) {
        switch gestureRecognizer.state {
        case .began:
            updateScrollViewHandler()
        case .changed:
            handleChange(ofGestureRecognizer: gestureRecognizer)
        case .cancelled, .ended:
            handleEnd(ofGestureRecognizer: gestureRecognizer)
            scrollViewHandler = nil
        default:
            break
        }
    }
    
    private func updateScrollViewHandler() {
        guard let child = presentedViewController.childViewControllers.first else {
            return
        }
        let topViewController = getTopViewController(of: child)
        self.scrollViewHandler = ScrollViewHandler(viewController: topViewController)
    }
    
    private func handleChange(ofGestureRecognizer gestureRecognizer: UIPanGestureRecognizer) {
        let progress = calculateProgress(gestureRecognizer)
        guard progress > 0 else {
            return
        }
        if transitionGreaterThanMaximum(progress)  {
            closeController(withProgress: progress)
            return
        }
        presentedView?.transform = presentedViewTransform(withProgress: progress)
        snapshot?.transform = snapshotTransform(withProgress: progress)
    }
    
    private func handleEnd(ofGestureRecognizer gestureRecognizer: UIPanGestureRecognizer) {
        let verticalVelocity = verticalVelocityInRootView(gestureRecognizer)
        let progress = calculateProgress(gestureRecognizer)
        if canBeFinished(withProgress: progress, verticalVelocity: verticalVelocity) {
            closeController(withProgress: progress)
        } else {
            animateToDefault()
        }
    }
    
    // MARK: helpers
    
    func calculateSnapshotScale() -> CGFloat {
        guard shouldScale else {
            return 1
        }
        let screenHeight = UIScreen.main.bounds.height
        let scale = (screenHeight - 2 * ( UIApplication.shared.statusBarFrame.height)) / screenHeight
        return scale
    }
    
    private func animateToDefault() {
        UIView.animate(withDuration: Constants.fastAnimationSpeed,
                       animations: {
                        self.presentedView?.transform = .identity
                        self.snapshot?.transform = self.snapshotDefaultTransform
        })
    }
    
    private func closeController(withProgress progress: CGFloat) {
        lastProgress = progress
        presentedViewController.dismiss(animated: true, completion: nil)
    }
    
    private func verticalVelocityInRootView(_ gestureRecognizer: UIPanGestureRecognizer) -> CGFloat {
        return gestureRecognizer.velocity(in: rootView).y
    }
    
    private func calculateProgress(_ gestureRecognizer: UIPanGestureRecognizer) -> CGFloat {
        if !isDismissEnabled {
            return 0
        }
        let translation = gestureRecognizer.translation(in: rootView).y - startLocation
        let progress = ((translation * Constants.transitionSlowCoefficient) / rootViewHeight)
        return CGFloat(fminf(fmaxf(Float(progress), 0.0), 1.0))
    }
    
    private func presentedViewTransform(withProgress progress: CGFloat) -> CGAffineTransform {
        let offset = progress * rootViewHeight
        return CGAffineTransform(translationX: 0, y: offset)
    }
    
    private func snapshotTransform(withProgress progress: CGFloat) -> CGAffineTransform {
        let scale = (1 - snapshotDefaultScale) * progress + snapshotDefaultScale
        return CGAffineTransform(scaleX: scale, y: scale)
    }

    private func canBeFinished(withProgress progress: CGFloat, verticalVelocity: CGFloat) -> Bool {
        return transitionGreaterThanMaximum(progress) ||
            (verticalVelocity > Constants.minimumVerticalValocity && progress > Constants.minimumProgressThreshold) ||
            (progress > Constants.centerProgressThreshold && verticalVelocity >= 0)
    }

    private func transitionGreaterThanMaximum(_ progress: CGFloat) -> Bool {
        return progress > Constants.maximumProgressThreshold
    }
    
    private func getTopViewController(of viewController: UIViewController) -> UIViewController {
        if let navigationController = viewController as? UINavigationController, let topViewController = navigationController.topViewController {
            return topViewController
        }
        return viewController
    }
    
    // MARK: status bar after  fix
    
    @objc
    private func updateStatusBar() {
        guard let containerView = containerView else { return }
        let fullHeight = containerView.window!.frame.size.height
        let newHeight = fullHeight - UIView.containerViewTopInset
        containerView.frame = CGRect(x: 0, y: UIView.containerViewTopInset, width: containerView.frame.width, height: newHeight)
    }
}

extension InteractionPresentationController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        guard gestureRecognizer.isEqual(panGesture) else {
            return false
        }
        return true
    }
}
