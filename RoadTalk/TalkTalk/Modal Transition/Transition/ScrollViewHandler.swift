//
//  ScrollDetector.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 28.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import UIKit

protocol ScrollViewProvider {
    var destinationScrollView: UIScrollView? { get }
}

class ScrollViewHandler {
    
    private(set) weak var scrollView: UIScrollView?
    private(set) var isDismissEnabled: Bool = false
    private(set) var startLocation: CGFloat?
    private var observation: NSKeyValueObservation?
    
    init?(viewController: UIViewController) {
    
        guard let scrollView = getScrollView(fromViewController: viewController) else {
            return nil
        }
        self.scrollView = scrollView
        observation = self.scrollView?.observe(\.contentOffset, options: [.initial]) { [weak self] _, _ in
            self?.scrollViewDidScroll()
        }
        self.scrollView?.panGestureRecognizer.addTarget(self, action: #selector(ScrollViewHandler.handleStartPosition(_:)))
    }
    
    deinit {
        scrollView?.panGestureRecognizer.removeTarget(self, action:  #selector(ScrollViewHandler.handleStartPosition(_:)))
        observation = nil
    }
    
    private func scrollViewDidScroll() {
        guard let scrollView = scrollView else {
            return
        }
        let offset: CGFloat = {
            if #available(iOS 11, *) {
                return scrollView.contentOffset.y + scrollView.safeAreaInsets.top
            } else {
                return scrollView.contentOffset.y
            }
        }()
        
        if offset > 0 {
            isDismissEnabled = false
            scrollView.bounces = true
        } else {
            if !scrollView.isDecelerating || scrollView.isTracking {
                isDismissEnabled = true
                scrollView.bounces = false
            }
        }
    }
    
    @objc private func handleStartPosition(_ gestureRecognizer: UIPanGestureRecognizer) {
        switch gestureRecognizer.state {
        case .began, .changed:
            setStartLocation()
        case .cancelled, .ended:
            startLocation = nil
        default:
            break
        }
    }
    
    private func setStartLocation() {
        guard let scrollView = scrollView else {
            return
        }
        if startLocation == nil {
            var topSafeArea: CGFloat = 0
            if #available(iOS 11, *) {
                topSafeArea = scrollView.safeAreaInsets.top
            }
            startLocation = scrollView.contentOffset.y + scrollView.contentInset.top + topSafeArea
        }
    }
    
    private func getScrollView(fromViewController viewController: UIViewController) -> UIScrollView? {
        if let scrollViewProvider = viewController as? ScrollViewProvider {
            return scrollViewProvider.destinationScrollView
        }
        if let scrollView = viewController.view as? UIScrollView {
            return scrollView
        }
        for subview in viewController.view.subviews {
            if let scrollView = subview as? UIScrollView {
                return scrollView
            }
        }
        return nil
    }
}
