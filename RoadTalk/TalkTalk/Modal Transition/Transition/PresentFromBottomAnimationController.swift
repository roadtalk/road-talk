//
//  PresentFromBottomAnimationController.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 28.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import UIKit

class BaseAnimationController: NSObject {
    
    let duration: TimeInterval
    
    init(duration: TimeInterval) {
        self.duration = duration
        super.init()
    }
}

class PresentFromBottomAnimationController: BaseAnimationController, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let presentedViewController = transitionContext.viewController(forKey: .to),
              let presentingViewController = transitionContext.viewController(forKey: .from) else {
            return
        }
        
        let containerView = transitionContext.containerView
        containerView.addSubview(presentedViewController.view)
        presentedViewController.view.frame = CGRect(x: 0, y: containerView.bounds.height, width: containerView.bounds.width, height: containerView.bounds.height)
        
        let finalFrameForPresentedView = transitionContext.finalFrame(for: presentedViewController)
        presentingViewController.beginAppearanceTransition(false, animated: true)
        UIView.animate(
            withDuration: transitionDuration(using: transitionContext),
            delay: 0,
            options: .curveEaseOut,
            animations: {
                presentedViewController.view.frame = finalFrameForPresentedView
        }, completion: { finished in
            presentingViewController.endAppearanceTransition()
            transitionContext.completeTransition(finished)
        })
    }
}

class DismissFromBottomAnimationController: BaseAnimationController, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let presentedViewController = transitionContext.viewController(forKey: .from),
              let presentingViewController = transitionContext.viewController(forKey: .to) else {
            return
        }
        
        let containerView = transitionContext.containerView
        let offscreenFrame = CGRect(x: 0, y: containerView.bounds.height, width: containerView.bounds.width, height: containerView.bounds.height)
        presentingViewController.beginAppearanceTransition(true, animated: true)
        UIView.animate(
            withDuration: transitionDuration(using: transitionContext),
            delay: 0,
            options: .curveEaseOut,
            animations: {
                presentedViewController.view.frame = offscreenFrame
        }, completion: { finished in
            presentingViewController.endAppearanceTransition()
            transitionContext.completeTransition(finished)
        })
    }
}
