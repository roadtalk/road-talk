//
//  PresentingTransitionDelegate.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 28.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import UIKit

class PresentingTransitionDelegate: NSObject, UIViewControllerTransitioningDelegate {
    
    private let configuration: PresentAnimationConfiguration
    private weak var interactionPresentationController: InteractionPresentationController?
    
    init(configuration: PresentAnimationConfiguration) {
        self.configuration = configuration
        super.init()
    }
    
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController)
        -> UIViewControllerAnimatedTransitioning? {
            return PresentFromBottomAnimationController(duration: configuration.duration)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let duration = interactionPresentationController?.timeToClose ?? configuration.duration
        return DismissFromBottomAnimationController(duration: duration)
    }
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        let interactionPresentationController = InteractionPresentationController(presentedViewController: presented,
                                                                                  presenting: presenting,
                                                                                  configuration: configuration)
        self.interactionPresentationController = interactionPresentationController
        return interactionPresentationController
    }
}
