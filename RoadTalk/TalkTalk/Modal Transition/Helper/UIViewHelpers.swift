//
//  UIViewHelpers.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 28.07.2018.
//

import UIKit

internal extension UIView {
    func pin(toSuperview: NSLayoutAttribute, offset: CGFloat = 0) {
        NSLayoutConstraint(item: self,
                           attribute: toSuperview,
                           relatedBy: .equal,
                           toItem: superview,
                           attribute: toSuperview,
                           multiplier: 1,
                           constant: offset).isActive = true
    }
    
    func setDimension(_ attribute: NSLayoutAttribute, equalTo to: CGFloat) {
        NSLayoutConstraint(item: self,
                           attribute: attribute,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 1,
                           constant: to).isActive = true
    }
    
    static var containerViewTopInset: CGFloat {
        let statusBarHeight = UIApplication.shared.statusBarFrame.height
        return statusBarHeight == 40 ? 20 : 0
    }
}

extension UIView {
    func addCornerRadiusAnimation(from: CGFloat, to: CGFloat, duration: CFTimeInterval) {
        let animation = CABasicAnimation(keyPath: #keyPath(CALayer.cornerRadius))
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.fromValue = from
        animation.toValue = to
        animation.duration = duration
        layer.add(animation, forKey: #keyPath(CALayer.cornerRadius))
        layer.cornerRadius = to
    }
}
