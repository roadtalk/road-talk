//
//  Bundle.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 28.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import UIKit

extension Bundle {
    
    static func currentBundle() -> Bundle {
        let bundle = Bundle(for: TransitionContainerViewController.self)
        if let path = bundle.path(forResource: "ModalTransition", ofType: "bundle") {
            return Bundle(path: path)!
        } else {
            return bundle
        }
    }
}
