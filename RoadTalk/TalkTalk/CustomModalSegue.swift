//
//  MapCustomModalSegue.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 28.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation
import UIKit

private struct Constansts {
    static let defaultTopOffset: CGFloat = UIScreen.main.bounds.size.height/2.8
    static let minimumControllerHeight: CGFloat = 390
}

public class CustomModalSegue: UIStoryboardSegue {
    override public func perform() {
        
        var customConfiguration = TransitionConfiguration.default
        
        let offset = source.view.frame.height - Constansts.defaultTopOffset > Constansts.minimumControllerHeight
            ? Constansts.defaultTopOffset
            : source.view.frame.height - Constansts.minimumControllerHeight
        
        customConfiguration.topOffset = offset
        customConfiguration.gapHeight = 0
        customConfiguration.prefferedStatusBarStyle = .default
        
        var presentAnimationConfiguration = PresentAnimationConfiguration.default
        presentAnimationConfiguration.shouldScale = false
        
        customConfiguration.presentAnimationConfiguration = presentAnimationConfiguration
        
        let transitionHelp = TransitionContainerViewController(targetViewController: destination,
                                                               configuration: customConfiguration)
        source.present(transitionHelp, animated: true, completion: nil)
    }
}
