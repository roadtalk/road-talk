//
//  Toolbar.swift
//  TalkTalk
//
//  Created by Mikhail Motyzhenkov on 30/07/2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation
import JSQMessagesViewController

extension JSQMessagesInputToolbar {
    func setup() {
        self.contentView.leftBarButtonItemWidth = 0
        let color = UIColor(red: 205/255.0, green: 206/255.0, blue: 210/255.0, alpha: 1)
        let textView = self.contentView.textView!
        textView.font = UIFont.systemFont(ofSize: 15)
        textView.placeHolderTextColor = color
        textView.backgroundColor = UIColor(white: 243/255.0, alpha: 1)
        textView.layer.borderColor = color.cgColor
        textView.layer.cornerRadius = textView.frame.height/2
        textView.textContainerInset = UIEdgeInsets(top: 4, left: 8, bottom: 4, right: 8)
        
        self.contentView.rightBarButtonItem.setImage(#imageLiteral(resourceName: "send"), for: .normal)
        self.contentView.rightBarButtonItem.setTitle(nil, for: .normal)
    }
}

// The most perverted way to customize left inset of placeholder

@objc protocol JSQMessagesComposerTextViewPrivateProtocol {
    @objc func jsq_placeholderTextAttributes() -> NSDictionary
}

private let swizzling: (AnyClass, Selector, Selector) -> () = { forClass, originalSelector, swizzledSelector in
    let originalMethod = class_getInstanceMethod(forClass, originalSelector)
    let swizzledMethod = class_getInstanceMethod(forClass, swizzledSelector)
    method_exchangeImplementations(originalMethod!, swizzledMethod!)
}

extension JSQMessagesComposerTextView {
    
    // Call it in AppDelegate
    static let swizzleInit: Void = {
        let originalSelector = #selector(JSQMessagesComposerTextViewPrivateProtocol.jsq_placeholderTextAttributes)
        let swizzledSelector = #selector(swizzled_jsq_placeholderTextAttributes)
        swizzling(JSQMessagesComposerTextView.self, originalSelector, swizzledSelector)
    }()
    
    @objc func swizzled_jsq_placeholderTextAttributes() -> NSDictionary {
        let leftInset: CGFloat = 6
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = NSLineBreakMode.byTruncatingTail
        paragraphStyle.alignment = self.textAlignment
        paragraphStyle.firstLineHeadIndent = leftInset
        paragraphStyle.headIndent = leftInset
        
        return [
            NSAttributedStringKey.font : self.font!,
            NSAttributedStringKey.foregroundColor : self.placeHolderTextColor,
            NSAttributedStringKey.paragraphStyle : paragraphStyle
                ]
    }
    
}
