//
//  ChatViewController.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 28.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation
import UIKit
import JSQMessagesViewController

protocol PostHolder: class {
    var post: PostResponse? { get set }
}

final class ChatViewController: JSQMessagesViewController, GoServiceHolder, FacebookDataServiceHolder, PostHolder {
    
    var goService: GoServiceProtocol!
    var facebookService: FacebookDataService!
    
    var post: PostResponse? = nil
    var messages = [JSQMessage]()
    var timer: Timer!
    let emptyImage = UIImage()
    
    lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = JSQMessagesBubbleImageFactory()!.outgoingMessagesBubbleImage(with: UIColor(red: 217/255.0, green: 228/255.0, blue: 247/255.0, alpha: 1))
    lazy var incomingBubbleImageView: JSQMessagesBubbleImage = JSQMessagesBubbleImageFactory()!.incomingMessagesBubbleImage(with: UIColor(red: 236/255.0, green: 237/255.0, blue: 240/255.0, alpha: 1))
    
    fileprivate func updateMessages(_ res: ([Comment])) {
        guard let post = post else { fatalError() }
        self.messages = res.map { JSQMessage(senderId: $0.facebookId, senderDisplayName: $0.displayName, date: $0.date, text: $0.message)}
        self.messages.insert(JSQMessage(senderId: post.facebookId, displayName: post.displayName, text: post.message), at: 0)
        self.finishReceivingMessage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inputToolbar.setup()
        self.inputToolbar.contentView.textView.placeHolder = "Type message"
        self.senderId = facebookService.senderId!
        guard let post = post else { fatalError() }
        goService.comments(param: CommentsInputParam(postId: post.postId)) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .result(let res):
                    self.updateMessages(res)
                case .fail(let error):
                    self.showError(message: error.localizedDescription, handler: nil)
                }
            }
        }
        self.collectionView.collectionViewLayout.messageBubbleFont = UIFont.systemFont(ofSize: 15)
        self.senderDisplayName = "Test name"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer.invalidate()
        timer = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        timer = Timer.scheduledTimer(withTimeInterval: 2, repeats: true, block: { [weak self] (timer)  in
            guard let this = self else { return }
            guard let post = this.post else { fatalError() }
            this.goService.comments(param: CommentsInputParam(postId: post.postId), completion: { (result) in
                DispatchQueue.main.async {
                    switch result {
                    case .result(let res):
                        this.updateMessages(res)
                    case .fail(let error):
                        this.showError(message: error.localizedDescription, handler: nil)
                    }
                }
            })
        })
    }
    
    private func addMessage(senderId: String, name: String, text: String) {
        if let message = JSQMessage(senderId: senderId, displayName: name, text: text) {
            messages.append(message)
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]
        if message.senderId == senderId {
            return outgoingBubbleImageView
        } else {
            return incomingBubbleImageView
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        let message = messages[indexPath.item]
        if let image = facebookService.getAvatar(facebookId: message.senderId) {
            return Avatar(image: image)
        }
        return Avatar(image: emptyImage)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        cell.textView?.textColor = UIColor.black
        return cell
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        guard let post = post else { fatalError() }
        addMessage(senderId: senderId, name: senderDisplayName, text: text)
        goService.addComment(param: AddCommentInputParam(postId: post.postId, message: text)) { (result) in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    break
                case .fail(let error):
                    self.showError(message: error.localizedDescription, handler: nil)
                }
            }
        }
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        finishSendingMessage()
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        let currentMessage = messages[indexPath.item]
        if currentMessage.senderId == senderId {
            return 0
        }
        if indexPath.item - 1 > 0 {
            let previousMessage = self.messages[indexPath.item - 1];
            if previousMessage.senderId == currentMessage.senderId {
                return 0
            }
        }
        return kJSQMessagesCollectionViewCellLabelHeightDefault
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAt indexPath: IndexPath!) -> CGFloat {
        if indexPath.item % 3 == 0 {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        }
        
        return 0
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        if indexPath.item % 3 == 0 {
            let message = self.messages[indexPath.item]
            return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
        }
        
        return nil;
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let currentMessage = messages[indexPath.item]
        if currentMessage.senderId == senderId {
            return nil
        }
        if indexPath.item - 1 > 0 {
            let previousMessage = self.messages[indexPath.item - 1]
            if previousMessage.senderId == currentMessage.senderId {
                return nil
            }
        }
        return NSAttributedString(string: currentMessage.senderDisplayName)
    }
    
}
