//
//  Avatar.swift
//  TalkTalk
//
//  Created by Mikhail Motyzhenkov on 28/07/2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation
import JSQMessagesViewController

final class Avatar: NSObject, JSQMessageAvatarImageDataSource {
    
    private let image: UIImage!
    
    init(image: UIImage) {
        self.image = image
        super.init()
    }
    
    func avatarImage() -> UIImage! {
        return image
    }
    
    func avatarHighlightedImage() -> UIImage! {
        return image
    }
    
    func avatarPlaceholderImage() -> UIImage! {
        return image
    }
    
    
}
