//
//  ChatContainerViewController.swift
//  TalkTalk
//
//  Created by Михаил Мотыженков on 29.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import UIKit

final class ChatContainerViewController: UIViewController, GoServiceHolder, FacebookDataServiceHolder, PostHolder {
    
    var goService: GoServiceProtocol!
    var facebookService: FacebookDataService!
    var post: PostResponse? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? GoServiceHolder {
            vc.goService = self.goService
        }
        if let vc = segue.destination as? FacebookDataServiceHolder {
            vc.facebookService = self.facebookService
        }
        if let vc = segue.destination as? PostHolder {
            vc.post = self.post
        }
    }

}
