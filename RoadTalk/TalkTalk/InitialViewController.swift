//
//  ViewController.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 28.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import UIKit

final class InitialViewController: UIViewController, GoServiceHolder, FacebookDataServiceHolder {

    private let loginSegue = "login"
    private let mapSegue = "map"
    private let chatSegue = "chat"
    
    var goService: GoServiceProtocol!
    var facebookService: FacebookDataService!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let user = Storage.user()
        goService = GoService(userId: user?.userId)
        facebookService = FacebookDataService(facebookId: Storage.facebookId())
        self.performSegue(withIdentifier: user == nil ? loginSegue: mapSegue , sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? GoServiceHolder {
            vc.goService = self.goService
        }
        if let vc = segue.destination as? FacebookDataServiceHolder {
            vc.facebookService = self.facebookService
        }
    }

}

