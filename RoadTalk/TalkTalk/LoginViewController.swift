//
//  LoginViewController.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 28.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation
import UIKit
import FacebookCore
import FacebookLogin

final class LoginViewController: UIViewController, GoServiceHolder, FacebookDataServiceHolder {
    
    private let mapSegue = "map"
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    
    private var isFirstEnter = true
    
    var goService: GoServiceProtocol!
    var facebookService: FacebookDataService!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.alpha = 0;
        subtitleLabel.alpha = 0;
        loginButton.alpha = 0;
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isFirstEnter {
            UIView.animate(withDuration: 1.2, delay: 0.5, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
                self.topConstraint.constant = -100
                self.view.layoutIfNeeded()
            })
            
            UIView.animate(withDuration: 2, delay: 2, options: .curveEaseOut, animations: {
                self.titleLabel.alpha = 1;
            }, completion: nil)
            
            UIView.animate(withDuration: 2, delay: 3, options: .curveEaseOut, animations: {
                self.subtitleLabel.alpha = 1;
            }, completion: nil)
            
            UIView.animate(withDuration: 2, delay: 4, options: .curveEaseOut, animations: {
                self.loginButton.alpha = 1;
            }, completion: nil)
            
            isFirstEnter = false
        }
       
    }
    
    @IBAction func loginFacebookTouchUp(_ sender: Any) {
        self.loginButton.isEnabled = false
        let loginManager = LoginManager()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        loginManager.logIn(readPermissions: [ .publicProfile ], viewController: self) { loginResult in
            
            switch loginResult {
            case .failed(let error):
                print(error)
                self.loginButton.isEnabled = true
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self.showError(message: error.localizedDescription)
            case .cancelled:
                print("User cancelled login.")
                self.loginButton.isEnabled = true
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            case .success( _, _, let accessToken):
                print("facebook Logged in!")
                let facebookId = accessToken.userId!
                let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"first_name"])
                graphRequest.start(completionHandler: { [weak self] (connection, result, error) in
                    guard let `self` = self else {
                        return
                    }
                    
                    if let err = error {
                        self.showError(message: err.localizedDescription)
                        self.loginButton.isEnabled = true
                    } else {
                        if let data = result as? [String : AnyObject], let name: String = data["first_name"] as? String {
                            Storage.save(facebookId: facebookId)
                            self.facebookService.senderId = facebookId
                            self.goService.register(param: RegisterInputParam(facebookId: facebookId, displayName: name), completion: { [weak self] (result) in
                                guard let `self` = self else {
                                    return
                                }
                                DispatchQueue.main.async {
                                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                    switch result {
                                    case .result(let registerResult):
                                        let user = Storage.User(facebookId: facebookId, userId: registerResult.userId)
                                        Storage.save(user: user)
                                        self.goService.userId = registerResult.userId
                                        self.performSegue(withIdentifier: self.mapSegue, sender: self)
                                    case .fail(let error):
                                        self.showError(message: error.localizedDescription)
                                        self.loginButton.isEnabled = true
                                        break
                                    }
                                }
                            })
                        }
                    }
                })
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? GoServiceHolder {
            vc.goService = self.goService
        }
        if let vc = segue.destination as? FacebookDataServiceHolder {
            vc.facebookService = self.facebookService
        }
    }
    
}
