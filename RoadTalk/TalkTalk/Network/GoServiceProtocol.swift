//
//  GoService.swift
//  TalkTalk
//
//  Created by Mikhail Motyzhenkov on 28/07/2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation

struct PostResponse: Codable, Equatable {
    var postId: String
    var latitude: Double
    var longitude: Double
    var message: String
    var typeId: TalkType
    var date: Date
    var displayName:String
    var facebookId:String
    
    public static func == (lhs: PostResponse, rhs: PostResponse) -> Bool {
        return lhs.postId == rhs.postId
    }
}

struct PostsInputParam: Codable {
    var latitude: Double
    var longitude: Double
    var radius: Int
}

struct PostsInputParamWithUserId: Codable {
    var userId: String
    var latitude: Double
    var longitude: Double
    var radius: Int
    
    init(param: PostsInputParam, userId: String) {
        self.userId = userId
        self.latitude = param.latitude
        self.longitude = param.longitude
        self.radius = param.radius
    }
}

struct CommentsInputParam: Codable {
    var postId: String
}

struct CommentsInputParamWithUserId: Codable {
    var userId: String
    
    init(userId: String) {
        self.userId = userId
    }
}

struct Comment: Codable {
    var facebookId: String
    var message: String
    var displayName: String
    var date: Date
}

struct AddPostInputParam: Codable {
    var latitude: Double
    var longitude: Double
    var message: String
    var typeId: TalkType
}

struct AddPostInputParamWithUserId: Codable {
    var userId: String
    var latitude: Double
    var longitude: Double
    var message: String
    var typeId: TalkType
    
    init(param: AddPostInputParam, userId: String) {
        self.userId = userId
        self.latitude = param.latitude
        self.longitude = param.longitude
        self.message = param.message
        self.typeId = param.typeId
    }
}

struct AddCommentInputParam: Codable {
    var postId: String
    var message: String
}

struct AddCommentInputParamWithUserId: Codable {
    var postId: String
    var userId: String
    var message: String
    
    init(param: AddCommentInputParam, userId: String) {
        self.userId = userId
        self.postId = param.postId
        self.message = param.message
    }
}

struct RegisterInputParam: Codable {
    var facebookId: String
    var displayName:String
}

struct RegisterResult: Codable {
    var userId: String
}

enum TalkType: String, Codable {
    case conversationGreen
    case conversationBlue
    case conversationPurple
    case conversationYellow
    case conversationRed

    case accident
    case roadWork
    case camera
    
    static func randomConversation() -> TalkType {
        //count types + 1!!!
        let rand = arc4random_uniform(6)
        switch rand {
        case 0:
            return .conversationPurple
        case 1:
            return .conversationRed
        case 2:
            return .conversationBlue
        case 3:
            return .conversationGreen
        case 4:
            return .conversationYellow
             //TODO: Add later
//        case 5:
//            return .accident
//        case 6:
//            return .roadWork
//        case 7:
//            return .camera
//        case 8:
//            return .other
        default:
            return .conversationYellow
        }
    }
}

protocol GoServiceProtocol {
    
    var userId: String? { get set }
    
    func register(param: RegisterInputParam, completion: @escaping (ServiceResult<RegisterResult>) -> Void)
    func posts(param: PostsInputParam, completion: @escaping (ServiceResult<[PostResponse]>) -> Void)
    func comments(param: CommentsInputParam, completion: @escaping (ServiceResult<[Comment]>) -> Void)
    func addPost(param: AddPostInputParam, completion: @escaping (EmptyServiceResult) -> Void)
    func addComment(param: AddCommentInputParam, completion: @escaping (EmptyServiceResult) -> Void)
}
