//
//  CommonTypes.swift
//  TalkTalk
//
//  Created by Mikhail Motyzhenkov on 28/07/2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation

enum ServiceResult<T> {
    case result(T)
    case fail(Error)
}

enum EmptyServiceResult {
    case success
    case fail(Error)
}

protocol GoServiceHolder: class {
    var goService: GoServiceProtocol! { get set }
}

protocol FacebookDataServiceHolder: class {
    var facebookService: FacebookDataService! { get set }
}
