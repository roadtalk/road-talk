//
//  GoServiceMock.swift
//  TalkTalk
//
//  Created by Mikhail Motyzhenkov on 28/07/2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation

final class GoServiceMock: GoServiceProtocol {
    
    var userId: String? = nil
    
    func register(param: RegisterInputParam, completion: @escaping (ServiceResult<RegisterResult>) -> Void) {
        completion(.result(RegisterResult(userId: "100003333214427")))
    }
    
    func posts(param: PostsInputParam, completion: @escaping (ServiceResult<[PostResponse]>) -> Void) {
        let result = [PostResponse(postId: "1", latitude: 51.5033640, longitude: -0.1276250, message: "Опять Собянин пол Москвы перекопал", typeId: .conversationBlue, date: Date(timeIntervalSinceNow: -120), displayName: "1", facebookId: "id_1"),
                      PostResponse(postId: "1", latitude: 51.4033640, longitude: -0.2276250, message: "😀 😁 😂 🤣 😃", typeId: .conversationRed, date: Date(timeIntervalSinceNow: -60),  displayName: "2", facebookId: "id_2")]
        completion(.result(result))
    }
    
    func comments(param: CommentsInputParam, completion: @escaping (ServiceResult<[Comment]>) -> Void) {
        let result = [Comment(facebookId: "100003333214427", message: "фдыжлвоаыджа 😀 😁", displayName: "Bill Gates", date: Date(timeIntervalSinceNow: -3600)),
                      Comment(facebookId: "100003333214427", message: "11123123", displayName: "Steve Jobs", date: Date(timeIntervalSinceNow: -200))]
        completion(.result(result))
    }
    
    func addPost(param: AddPostInputParam, completion: @escaping (EmptyServiceResult) -> Void) {
        completion(.success)
    }
    
    func addComment(param: AddCommentInputParam, completion: @escaping (EmptyServiceResult) -> Void) {
        completion(.success)
    }
}
