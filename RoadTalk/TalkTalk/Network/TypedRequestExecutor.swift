////
////  TypedRequestExecutor.swift
////  TalkTalk
//
//  Created by Mikhail Motyzhenkov on 28/07/2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case GET
    case POST
    case PUT
    case DELETE
}

protocol DataCoderProtocol {
    func encode<T: Encodable>(object: T) -> Any?
    func encodeToData<T: Encodable>(object: T) -> Data?
    func decode<T: Decodable>(body: Any) throws -> T
}

protocol TypedRequestExecutor {
    var dataCoder: DataCoderProtocol { get }
    var session: URLSession { get }
    func execute<Enc, Dec: Decodable>(param: ExecuteParam<Enc>, completion: @escaping (ServiceResult<Dec>) -> Void)
}

struct ExecuteParam<T: Encodable> {
    var url: String
    var body: T?
    var httpMethod: HTTPMethod
}

extension TypedRequestExecutor {
    
    private func request<T: Encodable>(param: ExecuteParam<T>) -> URLRequest {
        var request = URLRequest(url: URL(string: param.url)!)
        request.httpMethod = param.httpMethod.rawValue
        if let encodable = param.body {
            request.httpBody = dataCoder.encodeToData(object: encodable)
        }
        log(request: request)
        return request
    }

    func execute<Enc: Encodable, Dec: Decodable>(param: ExecuteParam<Enc>, completion: @escaping (ServiceResult<Dec>) -> Void) {
        let urlRequest = request(param: param)
        session.dataTask(with: urlRequest) { (data, urlResponse, error) in
            if let e = error {
                completion(.fail(e))
                return
            }
            guard let response = urlResponse as? HTTPURLResponse else {
                completion(.fail(NSError(domain: "NotHTTP", code: -4, userInfo: nil)))
                return
            }
            self.log(data: data, response: response, error: error)
            guard response.statusCode < 300 else {
                completion(.fail(NSError(domain: "Server error", code: response.statusCode, userInfo: nil)))
                return
            }
            guard let data: Data = data else {
                completion(.fail(NSError(domain: "NullData", code: -1, userInfo: nil)))
                return
            }
            let newData = data.count == 0 ? "{}".data(using: .utf8)! : data
            guard let decodable: Dec = try? self.dataCoder.decode(body: newData) else {
                completion(.fail(NSError(domain: "Decoding", code: -2, userInfo: nil)))
                return
            }
            completion(.result(decodable))
        }.resume()
    }
    
    func execute<Enc: Encodable>(param: ExecuteParam<Enc>, completion: @escaping (EmptyServiceResult) -> Void) {
        let urlRequest = request(param: param)
        session.dataTask(with: urlRequest) { (data, urlResponse, error) in
            if let e = error {
                completion(.fail(e))
                return
            }
            guard let response = urlResponse as? HTTPURLResponse else {
                completion(.fail(NSError(domain: "NotHTTP", code: -4, userInfo: nil)))
                return
            }
            self.log(data: data, response: response, error: error)
            guard response.statusCode < 300 else {
                completion(.fail(NSError(domain: "Server error", code: response.statusCode, userInfo: nil)))
                return
            }
            completion(.success)
        }.resume()
    }
}

extension TypedRequestExecutor {
    
    func log(request: URLRequest) {
        
        let urlString = request.url?.absoluteString ?? ""
        let components = NSURLComponents(string: urlString)
        
        let method = request.httpMethod != nil ? "\(request.httpMethod!)": ""
        let path = "\(components?.path ?? "")"
        let query = "\(components?.query ?? "")"
        let host = "\(components?.host ?? "")"
        
        var requestLog = "\n---------- OUT ---------->\n"
        requestLog += "\(urlString)"
        requestLog += "\n\n"
        requestLog += "\(method) \(path)?\(query) HTTP/1.1\n"
        requestLog += "Host: \(host)\n"
        for (key,value) in request.allHTTPHeaderFields ?? [:] {
            requestLog += "\(key): \(value)\n"
        }
        if let body = request.httpBody{
            requestLog += "\n\(NSString(data: body, encoding: String.Encoding.utf8.rawValue)!)\n"
        }
        
        requestLog += "\n------------------------->\n";
        print(requestLog)
    }
    
    func log(data: Data?, response: HTTPURLResponse?, error: Error?) {
        
        let urlString = response?.url?.absoluteString
        let components = NSURLComponents(string: urlString ?? "")
        
        let path = "\(components?.path ?? "")"
        let query = "\(components?.query ?? "")"
        
        var responseLog = "\n<---------- IN ----------\n"
        if let urlString = urlString {
            responseLog += "\(urlString)"
            responseLog += "\n\n"
        }
        
        if let statusCode =  response?.statusCode{
            responseLog += "HTTP \(statusCode) \(path)?\(query)\n"
        }
        if let host = components?.host{
            responseLog += "Host: \(host)\n"
        }
        for (key,value) in response?.allHeaderFields ?? [:] {
            responseLog += "\(key): \(value)\n"
        }
        if let body = data{
            responseLog += "\n\(NSString(data: body, encoding: String.Encoding.utf8.rawValue)!)\n"
        }
        if error != nil{
            responseLog += "\nError: \(error!.localizedDescription)\n"
        }
        
        responseLog += "<------------------------\n";
        print(responseLog)
    }
}
