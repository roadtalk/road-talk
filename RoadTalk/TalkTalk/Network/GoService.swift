//
//  GoService.swift
//  TalkTalk
//
//  Created by Mikhail Motyzhenkov on 28/07/2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation

func missingUserIdError() -> Error {
    return NSError(domain: "userId", code: -3, userInfo: [NSLocalizedDescriptionKey: "UserID is nil"])
}

final class GoService: GoServiceProtocol, TypedRequestExecutor {
    
    let session = URLSession(configuration: .default)
    let dataCoder: DataCoderProtocol = JSONCoder()
    
    let baseUrl = "http://159.69.10.125:3000/"
    var userId: String?
    
    init(userId: String?) {
        self.userId = userId
    }

    func register(param: RegisterInputParam, completion: @escaping (ServiceResult<RegisterResult>) -> Void) {
        execute(param: ExecuteParam(url: baseUrl + "reg", body: param, httpMethod: HTTPMethod.POST), completion: completion)
    }

    func posts(param: PostsInputParam, completion: @escaping (ServiceResult<[PostResponse]>) -> Void) {
        guard let userId = self.userId else {
            completion(.fail(missingUserIdError()))
            return
        }
        execute(param: ExecuteParam(url: baseUrl + "posts", body: PostsInputParamWithUserId(param: param, userId: userId), httpMethod: .POST), completion: completion)
    }

    func comments(param: CommentsInputParam, completion: @escaping (ServiceResult<[Comment]>) -> Void) {
        guard let userId = self.userId else {
            completion(.fail(missingUserIdError()))
            return
        }
        execute(param: ExecuteParam(url: "\(baseUrl)comments/\(param.postId)", body: CommentsInputParamWithUserId(userId: userId), httpMethod: .POST), completion: completion)
    }

    func addPost(param: AddPostInputParam, completion: @escaping (EmptyServiceResult) -> Void) {
        guard let userId = self.userId else {
            completion(.fail(missingUserIdError()))
            return
        }
        execute(param: ExecuteParam(url: baseUrl + "addpost", body: AddPostInputParamWithUserId(param: param, userId: userId), httpMethod: .POST), completion: completion)
    }

    func addComment(param: AddCommentInputParam, completion: @escaping (EmptyServiceResult) -> Void) {
        guard let userId = self.userId else {
            completion(.fail(missingUserIdError()))
            return
        }
        execute(param: ExecuteParam(url: baseUrl + "addcomment", body: AddCommentInputParamWithUserId(param: param, userId: userId), httpMethod: .POST), completion: completion)
    }
}
