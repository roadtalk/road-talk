//
//  FacebookDataService.swift
//  TalkTalk
//
//  Created by Михаил Мотыженков on 29.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation
import UIKit

final class FacebookDataService {
    
    let session = URLSession(configuration: .default)
    
    var senderId: String? = nil
    private var avatars = [String: UIImage]()
    
    func getAvatar(facebookId: String) -> UIImage? {
        if let image = avatars[facebookId], image.size != CGSize.zero {
            return image
        }
        avatars[facebookId] = UIImage()
        session.dataTask(with: URLRequest(url: URL(string: "https://graph.facebook.com/\(facebookId)/picture?type=small")!)) { (data, urlResponse, error) in
            if error != nil {
                return
            }
            guard let data = data, let image = UIImage(data: data) else {
                return
            }
            
            self.avatars[facebookId] = image.roundImage()
        }.resume()
        return nil
    }
    
    func getAsyncAvatar(facebookId: String, result:@escaping (UIImage)->()) {
        if let image = avatars[facebookId], image.size != CGSize.zero {
            result(image)
        }
        avatars[facebookId] = UIImage()
        session.dataTask(with: URLRequest(url: URL(string: "https://graph.facebook.com/\(facebookId)/picture?width=100&heigth=100")!)) { (data, urlResponse, error) in
            if error != nil {
                return
            }
            guard let data = data, let image = UIImage(data: data) else {
                return
            }
            
            self.avatars[facebookId] = image.roundImage()
            result(self.avatars[facebookId]!)
            }.resume()
        
    }
    
    init(facebookId: String?) {
        senderId = facebookId
    }
}

extension UIImage {
    func roundImage() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, UIScreen.main.scale)
        
        let rect = CGRect(origin: CGPoint.zero, size: self.size)
        UIBezierPath(roundedRect: rect, cornerRadius: self.size.height/2).addClip()
        self.draw(in: rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
