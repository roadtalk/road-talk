//
//  DataCodableService.swift
//  TalkTalk
//
//  Created by Mikhail Motyzhenkov on 28.07.18.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation

final class JSONCoder: DataCoderProtocol {
    
    var dateDecodingStrategy: JSONDecoder.DateDecodingStrategy = .customISO8601
    var dateEncodingStrategy: JSONEncoder.DateEncodingStrategy = .customISO8601
    
    func encode<T: Encodable>(object: T) -> Any? {
        
        var body: Any?
        if let data = encodeToData(object: object) {
            body = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
        }
        return body
    }
    
    func encodeToData<T: Encodable>(object: T) -> Data? {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = dateEncodingStrategy
        return try? encoder.encode(object)
    }
    
    func decode<T: Decodable>(body: Any) throws -> T {
        
        guard let data = body as? Data else {
            throw errorWithMessage("Decodable body is not of type Data")
        }
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = dateDecodingStrategy
        do {
            let object = try decoder.decode(T.self, from: data)
            return object
        } catch {
            throw errorWithMessage("Decoding error")
        }
    }
    
    private func errorWithMessage(_ message: String) -> NSError {
        return NSError(domain: "ru.qiwi.dataCoder", code: 1, userInfo: [NSLocalizedDescriptionKey: message])
    }
}

extension Formatter {
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        return formatter
    }()
    static let iso8601noFS: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXXXX"
        return formatter
    }()
}

extension JSONDecoder.DateDecodingStrategy {
    static let customISO8601 = custom { decoder throws -> Date in
        let container = try decoder.singleValueContainer()
        let string = try container.decode(String.self)
        if let date = Formatter.iso8601.date(from: string) ?? Formatter.iso8601noFS.date(from: string) {
            return date
        }
        throw DecodingError.dataCorruptedError(in: container, debugDescription: "Invalid date: \(string)")
    }
}

extension JSONEncoder.DateEncodingStrategy {
    static let customISO8601 = custom { date, encoder throws in
        var container = encoder.singleValueContainer()
        try container.encode(Formatter.iso8601.string(from: date))
    }
}
