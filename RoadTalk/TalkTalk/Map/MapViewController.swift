//
//  MapViewController.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 28.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation
import UIKit
import TomTomOnlineSDKMaps
import TomTomOnlineSDKRouting
import TomTomOnlineSDKMapsUIExtensions
import CoreLocation

final class MapViewController: UIViewController, GoServiceHolder, FacebookDataServiceHolder, PostHolder {

    enum State {
        case normal
        case creation(annotation:TTAnnotation)
    }
    
    private var currentState:State = .normal
    
    @IBOutlet weak var scrollView: MagicScrollView!
    private let zoom = 15.5
    @IBOutlet var mapView: TTMapView!
    @IBOutlet weak var controllView: TTControlView!
    
    private var needTrackUserLocation = true
    
    var goService: GoServiceProtocol!
    var facebookService: FacebookDataService!
    var post: PostResponse? = nil
    private var isLongPress = false
    private var creationTalkType:TalkType?
    private let annotationDelegate = AnnotationDelegate()
    private var annotationGenerator = AnnotationGenerator()
    
    
    @objc func showChat(notification:Notification) {
        
        self.post = notification.userInfo!["post"] as? PostResponse
        guard let post = self.post else { fatalError() }
        let position = CLLocationCoordinate2D(latitude: post.latitude - 0.03/self.mapView.zoom(), longitude: post.longitude)
        self.mapView.setCameraPosition(TTCameraPosition(camerPosition: position, withAnimationDuration: 0))
        self.mapView.center(on: position, withZoom: self.zoom)
        self.performSegue(withIdentifier: "chat", sender: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(showChat(notification:)), name: NSNotification.Name(rawValue: "open_chat"), object: nil)
        
        self.scrollView.facebookService = self.facebookService
        self.annotationGenerator.goService = self.goService
        
        self.mapView.onMapReadyCompletion { [weak self] in
            guard let `self` = self else {
                return
            }
            
            LocationManager.sharedInstance.reqeuestAccess()
            
            self.scrollView.mapView = self.mapView
            self.mapView.annotationManager.clustering = true
            self.mapView.annotationManager.delegate = self.annotationDelegate
            self.annotationGenerator.annotationManager = self.mapView.annotationManager
            self.annotationGenerator.mapView = self.mapView
            self.annotationGenerator.scroll = self.scrollView
            
            self.controllView.mapView = self.mapView
            self.controllView.initDefaultCenterButton()
            self.controllView.centerButton?.addTarget(self, action: #selector(MapViewController.didTapOnCenterButton), for: .touchUpInside)
            
//            self.mapView.minZoom = 13
//            self.mapView.maxZoom = 17.5
            self.mapView.delegate = self
            
            self.mapView.isShowsUserLocation = true
//            self.mapView.trafficTileStyle = TTRasterTileType.setStyle(.relative)

            LocationManager.sharedInstance.didUpdateUserLocation = { [weak self] userLocation in
                guard let `self` = self else {
                    return
                }

                if (self.needTrackUserLocation) {
                    self.needTrackUserLocation = false
                    self.mapView.center(on: userLocation, withZoom: self.zoom)
                }
            }
        }
    }
    
    @objc private func didTapOnCenterButton() {
        
        if let location = LocationManager.sharedInstance.userLocation {
            self.needTrackUserLocation = true
            self.mapView.center(on: location, withZoom: self.zoom)
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.controllView.centerButton?.isHidden = false
        switch self.currentState {
        case .normal:
            break
        case .creation(let annotation):
            self.mapView.annotationManager.remove(annotation)
            self.currentState = .normal 
        }
        
        self.annotationGenerator.start()
          self.navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.annotationGenerator.stop()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? GoServiceHolder {
            vc.goService = self.goService
        }
        if let vc = segue.destination as? FacebookDataServiceHolder {
            vc.facebookService = self.facebookService
        }
        if let vc = segue.destination as? PostHolder {
            vc.post = self.post
        }
        
        if let vc = segue.destination as? CreatePostViewController {
            switch self.currentState {
            case .normal:
                break
            case .creation(let annotation):
                vc.coordinate = annotation.coordinate
                vc.talkType = self.creationTalkType
            }
        }
        

    }
}

extension MapViewController:TTAnnotationDelegate {
    
}

extension MapViewController:TTMapViewDelegate {
    
    func mapView(_ mapView: TTMapView, didRotate angle: CGFloat) {
        //self.needTrackUserLocation = false
        isLongPress = false
    }
    
    func mapView(_ mapView: TTMapView, didPanning coordinate: CLLocationCoordinate2D) {
       
//        if isLongPress {
//            let annotation = TTAnnotation(coordinate: coordinate)
//            self.mapView.annotationManager.add(annotation)
//
//            self.mapView.center(on: coordinate)
//        }
//        isLongPress = false
//        print(self.mapView.zoom())
    }
    
    func mapView(_ mapView: TTMapView, didDoubleTap coordinate: CLLocationCoordinate2D) {
       // self.needTrackUserLocation = false
        isLongPress = false
    }
    
    func mapView(_ mapView: TTMapView, didLongPress coordinate: CLLocationCoordinate2D) {

        
     //  self.scrollView.show(posts: self.annotationGenerator.posts)
     //  return
        
        self.creationTalkType = TalkType.randomConversation()
        let imageName = "creation_pin"
        let annotation = TTAnnotation(coordinate: coordinate, image: UIImage(named: imageName)!, tag: imageName, anchor: .center, type: .focal)
        annotation.shouldCluster = false
        self.mapView.annotationManager.add(annotation)
     
        let position = CLLocationCoordinate2D(latitude: coordinate.latitude - 0.03/self.mapView.zoom(), longitude: coordinate.longitude)
        self.mapView.setCameraPosition(TTCameraPosition(camerPosition: position, withAnimationDuration: 0))
        self.mapView.center(on: position, withZoom: self.zoom)
        self.currentState = .creation(annotation: annotation)
        self.performSegue(withIdentifier: "add_post", sender: self)
        
        
        self.controllView.centerButton!.isHidden = true
    }
    
    func mapView(_ mapView: TTMapView, didSingleTap coordinate: CLLocationCoordinate2D) {
        
    }
    
}

