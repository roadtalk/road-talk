//
//  PostCell.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 31.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation

class PostCell:UIView {
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var title: UILabel!
    let post:PostResponse
    var view: UIView!
    
    init(post:PostResponse, frame:CGRect) {
        self.post = post
        super.init(frame: frame)
        xibSetup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        addSubview(view)
        
        self.isUserInteractionEnabled = true
        
        title.text = post.message
        subtitle.text = post.displayName + ", " + post.date.timeAgoSinceNow(useNumericDates: true)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(showChat))
        self.addGestureRecognizer(gesture)
    }
    
    @objc func showChat() {
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "open_chat"), object: nil, userInfo: ["post":self.post]))
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "PostCell", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }
}
