//
//  MagicScrollView.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 30.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class MagicScrollView:UIScrollView, UIScrollViewDelegate, FacebookDataServiceHolder {
    
    enum Mode {
        case normal
        case tappedOnPoint
    }
    
    var mapView:TTMapView?
    var views:[UIView] = []

    let itemHeight:CGFloat = 80;
    let headerHeight:CGFloat = 28;
    
    var footer:UIView = UIView()
    
    var facebookService: FacebookDataService!
    
    var currentMode:Mode = .normal
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.delegate = self
        footer.backgroundColor = UIColor.white
        
        NotificationCenter.default.addObserver(self, selector: #selector(didRecieve(notification:)), name: NSNotification.Name(rawValue: "show_single_post"), object: nil)
    }
    
    @objc func didRecieve(notification:Notification) {
        let annotation = notification.userInfo!["annotation"] as! Annotation
        let post = annotation.post
        self.currentMode = .tappedOnPoint
        
        self.show(posts: [post])
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        
        if let view = views.first {
            if point.y >= view.frame.origin.y {
                return super.hitTest(point, with: event)
            }
        }
        return nil
    }

    func topOffset(post:[PostResponse])->CGFloat {
        let screenHeight = UIScreen.main.bounds.size.height
        let countVisibleItems:Int = post.count >= 3 ? 3 : post.count
        let visibleItemsHeight:CGFloat = CGFloat(countVisibleItems) * itemHeight
        
        
        
        return screenHeight - (visibleItemsHeight) - headerHeight - 20 - deviceOffset()
    }
    
    func deviceOffset() ->CGFloat {
        if UIDevice().userInterfaceIdiom == .phone {
            
            if UIScreen.main.nativeBounds.height == 2436 {
                return 40
            }
        }
        return 0
    }
    
    func show(posts:[PostResponse]) {
        
        let needAnimation = self.views.count <= 1
        
        hidePosts()
        
        if posts.count == 0 {
            return
        }
        
        let topOffset = self.topOffset(post: posts)
        self.setContentOffset(CGPoint(x: 0, y: -topOffset), animated: false)
        
        
        let header = UIImageView(image: UIImage(named: "header"))
        self.addSubview(header)
        self.views.append(header)
        header.snp.makeConstraints { (maker) in
            maker.top.equalTo(self.snp.top).offset(topOffset)
            maker.height.equalTo(headerHeight)
            maker.width.equalTo(self)
        }
        
        var lastItem:UIView!
        for i in 0...posts.count - 1 {
            let view = PostCell(post: posts[i], frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: itemHeight))
            self.addSubview(view)
            if let avatar = self.facebookService.getAvatar(facebookId: posts[i].facebookId) {
                view.imageView.image = avatar
                
            } else {
                self.facebookService.getAsyncAvatar(facebookId: posts[i].facebookId, result: {[weak view] img in
                    DispatchQueue.main.async {
                        view?.imageView.image = img
                    }
                    
                })
            }
            
            
            view.snp.makeConstraints { (maker) in
                
                if (i == 0) {
                    maker.top.equalTo(header.snp.bottom).offset(-2)
                } else {
                    maker.top.equalTo(lastItem.snp.bottom).offset(0)
                }
                
                maker.height.equalTo(itemHeight)
                maker.width.equalTo(self)
                maker.left.equalTo(0)
            }
            views.append(view)
            lastItem = view
        }
        
        self.addSubview(footer)
        footer.snp.remakeConstraints { (maker) in
            
            maker.top.equalTo(lastItem.snp.bottom).offset(0)
            
            maker.height.equalTo(400)
            maker.width.equalTo(self)
            maker.left.equalTo(0)
        }
    
        
        self.contentSize = CGSize(width: self.bounds.size.width, height:topOffset + CGFloat(posts.count) * CGFloat(self.itemHeight) + headerHeight)
        self.setContentOffset(CGPoint.zero, animated: needAnimation)
        

    }
    
    func hidePosts() {
        for view in self.views {
            view.removeFromSuperview()
        }

        self.views = []
        footer.removeFromSuperview()
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    //delegate
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        print(self.contentOffset.y)
//        let countVisibleItems:Int = self.views.count - 1 >= 3 ? 3 : self.views.count - 1
//        let diff = CGFloat(self.views.count - 1 - countVisibleItems)
//        
//        if self.contentOffset.y >= diff*itemHeight + headerHeight {
//            self.setContentOffset(CGPoint(x: 0, y: diff*itemHeight), animated: false)
//        }
//    }
//    
}
