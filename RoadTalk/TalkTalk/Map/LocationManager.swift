//
//  CCLocationManager+Helpers.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 28.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation
import CoreLocation

final class LocationManager: NSObject, CLLocationManagerDelegate {
    
    static let sharedInstance = LocationManager()
    private let locManager:CLLocationManager = CLLocationManager()
    
    override init() {
        super.init()
        locManager.delegate = self
    }
    
    var userLocation:CLLocationCoordinate2D?
    
    var didUpdateUserLocation:((CLLocationCoordinate2D)->())? = nil
    
    func reqeuestAccess() {
        locManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let lat = locations.last?.coordinate.latitude, let long = locations.last?.coordinate.longitude {
            print("\(lat),\(long)")
            self.userLocation = locations.last!.coordinate
            didUpdateUserLocation?(self.userLocation!)
            
        } else {
            print("No coordinates")
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}
