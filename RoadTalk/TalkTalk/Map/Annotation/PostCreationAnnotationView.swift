//
//  PostCreationAnnotationView.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 30.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation
import UIKit
import TomTomOnlineSDKMaps

@objcMembers
class PostCreationAnnotationView:UIView, TTCalloutView {
    var annotation: TTAnnotation!
    init(annotation:TTAnnotation) {
        super.init(frame: CGRect.zero)
        self.annotation = annotation
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
