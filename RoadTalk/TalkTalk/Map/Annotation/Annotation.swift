//
//  AnnotationTag.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 30.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation
import TomTomOnlineSDKMaps

@objcMembers
class Annotation:TTAnnotation {
    enum NamePrefix:String {
        case post
        case post_selected
    }
    var post:PostResponse
    var namePrefix:NamePrefix
    var isNormal:Bool {
        get {
            return self.namePrefix == .post
        }
    }
    
    func convertToSelected() -> Annotation {
        return Annotation(post: post, namePrefix: .post_selected)
    }
    
    func convertToNormal() -> Annotation {
        return Annotation(post: post, namePrefix: .post)
    }
    
    init(post:PostResponse, namePrefix:NamePrefix) {
        self.post = post
        self.namePrefix = namePrefix
        
        let imageName = namePrefix.rawValue+"_" + post.typeId.rawValue
        let image = UIImage(named: imageName)!
        let coordinate = CLLocationCoordinate2D(latitude: post.latitude, longitude: post.longitude)
        super.init(coordinate: coordinate, image: image, tag: post.postId + "," + imageName, anchor: .center, type: .focal)
    }
    
    func addTo(manager:TTAnnotationManager) {
        manager.add(self)
    }
    
    func removeFrom(manager:TTAnnotationManager) {
        if manager.annotations.contains(self) {
            manager.remove(self)
        }
        
    }
}



