//
//  AnnotationService.h
//  TalkTalk
//
//  Created by Сергей Петрук on 30.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TomTomOnlineSDKMaps/TomTomOnlineSDKMaps.h>

@interface AnnotationDelegate : NSObject <TTAnnotationDelegate>

@end
