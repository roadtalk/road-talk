//
//  AnnotationService.m
//  TalkTalk
//
//  Created by Сергей Петрук on 30.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

#import "AnnotationDelegate.h"
#import <UIKit/UIKit.h>

#import "TalkTalk-Swift.h"

@interface AnnotationDelegate()

@property (nonatomic) Annotation *currentAnnotation;


@end

@implementation AnnotationDelegate

- (void)annotationManager:(TTAnnotationManager *)annotationManager annotationSelected:(TTAnnotation *)annotation {
    
    if (self.currentAnnotation) {
        Annotation *a = [self.currentAnnotation convertToNormal];
        [self.currentAnnotation removeFromManager:annotationManager];
        [a addToManager:annotationManager];
        self.currentAnnotation = nil;
    }
    
    if ([annotation isKindOfClass:[Annotation class]]) {
        Annotation *a = (id)annotation;
        [a removeFromManager:annotationManager];
        
        Annotation *a2 = [a convertToSelected];
        self.currentAnnotation = a2;
        [a2 addToManager:annotationManager];
    }
    
    if (self.currentAnnotation) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"show_single_post" object:nil userInfo:@{@"annotation":self.currentAnnotation}];
    }

}

- (UIView<TTCalloutView> *)annotationManager:(TTAnnotationManager *)annotationManager viewForSelectedAnnotation:(TTAnnotation *)selectedAnnotation {
    return [[PostCreationAnnotationView alloc] initWithAnnotation:selectedAnnotation];
}

#pragma MARK - not used

- (void)annotationManager:(TTAnnotationManager *)annotationManager touchUpCircle:(TTCircle *)circle {
    
}

- (void)annotationManager:(TTAnnotationManager *)annotationManager touchUpPolygon:(TTPolygon *)polygon {
    
}

- (void)annotationManager:(TTAnnotationManager *)annotationManager touchUpPolyline:(TTPolyline *)polyline {
    
}


@end
