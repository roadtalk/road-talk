//
//  AnnotationGenerator.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 30.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation
import UIKit
import TomTomOnlineSDKMaps

class AnnotationGenerator:GoServiceHolder {
    private let maxLength = 100
    private let radius = 1
    public var annotations:[Annotation] = []
    
    
    var goService: GoServiceProtocol!
    weak var annotationManager:TTAnnotationManager? {
        didSet {
            if let _ = self.annotationManager, let _ = self.mapView, let _ = self.scroll  {
                self.start()
            }
        }
    }
    
    weak var mapView: TTMapView? {
        didSet {
            if let _ = self.annotationManager, let _ = self.mapView, let _ = self.scroll  {
                self.start()
            }
        }
    }
    
    weak var scroll: MagicScrollView? {
        didSet {
            if let _ = self.annotationManager, let _ = self.mapView, let _ = self.scroll {
                self.start()
            }
        }
    }
    
    private var timer: Timer?
    
    public func start() {
        stop()
        
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { [weak self] (timer)  in
            guard let `self` = self else { return }
            let cameraPosition = self.mapView?.cameraPosition().cameraPosition
            if let center = self.mapView?.centerCoordinate() {
                self.goService.posts(param: PostsInputParam(latitude: center.latitude, longitude: center.longitude, radius: self.radius), completion: { (result) in
                    DispatchQueue.main.async {
                        switch result {
                        case .result(let res):
                            
                            let newPos = self.mapView!.cameraPosition().cameraPosition
                            print(cameraPosition, newPos)
                            let cameraUpdated = cameraPosition != nil ? (cameraPosition!.latitude != newPos.latitude) || (cameraPosition!.longitude != newPos.longitude) : false
                            let haveNew = self.add(posts: res)
                            
                            if haveNew && self.scroll!.currentMode == .normal {
                                self.scroll?.show(posts: self.annotations.map({$0.post}))
                            } else if cameraUpdated, self.scroll!.currentMode == .tappedOnPoint {
                                self.scroll!.currentMode = .normal
                                
                                self.annotationManager?.removeAllAnnotations()
                                self.annotations = []
                                self.add(posts: res)
                                self.scroll?.show(posts: self.annotations.map({$0.post}))
                            }
                            
                            
                            if self.add(posts: res), self.scroll!.currentMode == .normal {
                                self.scroll?.show(posts: self.annotations.map({$0.post}))
                            }
                            
                        case .fail(let error):
                            //this.showError(message: error.localizedDescription, handler: nil)
                            break
                        }
                    }
                })
            }
            
           
        })
    }
    
    public func add(posts:[PostResponse])->Bool {
        var old:[Annotation] = []
        var oldPosts:[PostResponse] = []
        var isNew = false
        

        for a in annotations {
            if posts.contains(a.post) {
                old.append(a)
                oldPosts.append(a.post)
            } else {
                if self.annotationManager!.annotations.contains(a) {
                    self.annotationManager?.remove(a)
                }
                
            }
        }
        let tPosts:[PostResponse] = posts
        let sortedPosts:[PostResponse] = tPosts.sorted { (p1, p2) -> Bool in
            return p1.date > p2.date
        }
        
        if sortedPosts != self.annotations.map({$0.post}) {
            isNew = true
        }
        self.annotations = old
        for p in posts {
            if !oldPosts.contains(p) {
                isNew = true
                if self.annotations.count > 0 {
                    var isAdded = false
                    for index in 0...self.annotations.count - 1 {
                        let an:Annotation = self.annotations[index]
                        if an.post.date < p.date {
                            let newAnnotation = Annotation(post: p, namePrefix: .post)
                            self.annotations.insert(newAnnotation, at: index)
                            self.annotationManager?.add(newAnnotation)
                            isAdded = true
                            break
                        }
                    }
                    
                    if !isAdded {
                        let newAnnotation = Annotation(post: p, namePrefix: .post)
                        self.annotations.append(newAnnotation)
                        self.annotationManager?.add(newAnnotation)
                    }
                } else {
                    let newAnnotation = Annotation(post: p, namePrefix: .post)
                    self.annotations.append(newAnnotation)
                    self.annotationManager?.add(newAnnotation)
                }
                
                
            }
        }
        return isNew
    }
    
//    public func addPost(post:PostResponse) ->Bool {
//        var isNew = false
//        if !self.posts.contains(post) {
//            isNew = true
//            if self.posts.count > 0 {
//                for index in 0...posts.count {
//                    let p:PostResponse = posts[index]
//                    if post.date > p.date {
//                        let newAnnotation = Annotation(post: post, namePrefix: .post)
//                        self.annotations.insert(newAnnotation, at: index)
//                        self.annotationManager?.add(newAnnotation)
//                    }
//                }
//            } else {
//                self.posts.append(post)
//                self.addAnnotation(post: post)
//            }
//
//        }
//        return isNew
//    }
    
    private func addAnnotation(post:PostResponse) {
        self.annotationManager?.add(Annotation(post: post, namePrefix: .post))
    }
    
    public func stop() {
        timer?.invalidate()
        timer = nil
    }
}
