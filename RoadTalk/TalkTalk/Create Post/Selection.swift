//
//  Selection.swift
//  TalkTalk
//
//  Created by Mikhail Motyzhenkov on 31/07/2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation
import UIKit

final class Selection: CAShapeLayer {
    
    override init(layer: Any) {
        super.init(layer: layer)
        path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: 50, height: 50)).cgPath
        self.strokeColor = UIColor(white: 231/255.0, alpha: 1).cgColor
        self.lineWidth = 2
        self.fillColor = UIColor.clear.cgColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
