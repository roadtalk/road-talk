//
//  HorizontalSelector.swift
//  TalkTalk
//
//  Created by Mikhail Motyzhenkov on 31/07/2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import UIKit

@IBDesignable
final class HorizontalSelector: UIView {
    
    @IBOutlet weak var buttonTalk: UIButton!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var stack: UIStackView!
    var selectionLayer: Selection!
    
    enum EventType {
        case accident
        case roadWork
        case camera
        case talk
    }
    var eventType = EventType.talk {
        didSet {
            eventTypeSelected?(eventType)
        }
    }
    var eventTypeSelected: ((EventType) -> Void)? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(contentView!)
        
        selectionLayer = Selection(layer: self.layer)
        layer.addSublayer(selectionLayer)
        self.showSelection(view: self.buttonTalk, eventType: .talk)
        selectionLayer.opacity = 0
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            self.showSelection(view: self.buttonTalk, eventType: .talk)
            self.selectionLayer.opacity = 1
        })
    }
    
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }
    
    private func showSelection(view: UIView, eventType: EventType) {
        let width: CGFloat = 50
        let height: CGFloat = 50
        selectionLayer.frame = CGRect(x: view.center.x-width/2, y: view.center.y-height/2, width: width, height: height)
        UIView.animate(withDuration: 0.25) {
            view.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            for other in self.stack.arrangedSubviews {
                if other != view {
                    other.transform = .identity
                }
            }
        }
        self.eventType = eventType
    }
    
    @IBAction func accidentTouched(_ sender: UIButton) {
        showSelection(view: sender, eventType: .accident)
    }
    
    @IBAction func roadWorkTouched(_ sender: UIButton) {
        showSelection(view: sender, eventType: .roadWork)
    }
    
    @IBAction func cameraTouched(_ sender: UIButton) {
        showSelection(view: sender, eventType: .camera)
    }
    
    @IBAction func talkTouched(_ sender: UIButton) {
        showSelection(view: sender, eventType: .talk)
    }
}
