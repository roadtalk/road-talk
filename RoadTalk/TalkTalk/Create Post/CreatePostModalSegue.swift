//
//  CreatePostModalSegue.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 29.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation
import UIKit

private struct Constansts {
    static let defaultTopOffset: CGFloat = UIScreen.main.bounds.size.height/3.3
    static let minimumControllerHeight: CGFloat = 464
}

public class CreatePostModalSegue: UIStoryboardSegue {
    override public func perform() {
        
        var customConfiguration = TransitionConfiguration.default
        
        let offset = source.view.frame.height - Constansts.defaultTopOffset > Constansts.minimumControllerHeight
            ? Constansts.defaultTopOffset
            : source.view.frame.height - Constansts.minimumControllerHeight
        
        customConfiguration.topOffset = offset
        customConfiguration.gapHeight = 0
        customConfiguration.prefferedStatusBarStyle = .default
        
        var presentAnimationConfiguration = PresentAnimationConfiguration.default
        presentAnimationConfiguration.shouldScale = false
        presentAnimationConfiguration.alpha = 1
        
        customConfiguration.presentAnimationConfiguration = presentAnimationConfiguration
        
        let transitionHelp = TransitionContainerViewController(targetViewController: destination,
                                                               configuration: customConfiguration)
        source.present(transitionHelp, animated: true, completion: nil)
    }
}
