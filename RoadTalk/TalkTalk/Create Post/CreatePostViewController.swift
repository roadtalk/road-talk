//
//  CreatePostViewController.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 29.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import JSQMessagesViewController

final class CreatePostViewController: UIViewController, GoServiceHolder {
    
    @IBOutlet weak var titleLabel: UILabel!
    var talkType:TalkType? = nil
    private var initialTalkType: TalkType!
    @IBOutlet weak var toolbar: JSQMessagesInputToolbar!
    @IBOutlet weak var horizontalSelector: HorizontalSelector!
    
    var goService: GoServiceProtocol!
    
    public var coordinate: CLLocationCoordinate2D!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        toolbar.setup()
        toolbar.contentView.textView.delegate = self
        toolbar.contentView.textView.placeHolder = "Create talk"
        toolbar.setShadowImage(UIImage(), forToolbarPosition: UIBarPosition.any)
        toolbar.delegate = self
        horizontalSelector.eventTypeSelected = { [unowned self] eventType in
            let text = eventType.toLabel()
            self.toolbar.contentView.textView.placeHolder = "Create " + text
            self.titleLabel.text = "New " + text
            self.talkType = self.eventTypeToTalkType(eventType: eventType)
        }
        initialTalkType = talkType!
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.toolbar.contentView.textView.becomeFirstResponder()
    }
    
    private func eventTypeToTalkType(eventType: HorizontalSelector.EventType) -> TalkType {
        switch eventType {
        case .accident:
            return .accident
        case .roadWork:
            return .roadWork
        case .camera:
            return .camera
        case .talk:
            return initialTalkType
        }
    }
}

extension HorizontalSelector.EventType {
    func toLabel() -> String {
        switch self {
        case .accident:
            return "traffic jam"
        case .roadWork:
            return "roadworks"
        case .camera:
            return "camera"
        case .talk:
            return "talk"
        }
    }
}

extension CreatePostViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        let isEnabled = !textView.text.isEmpty
        if toolbar.contentView.rightBarButtonItem.isEnabled != isEnabled {
            toolbar.contentView.rightBarButtonItem.isEnabled = isEnabled
        }
    }
}

extension CreatePostViewController: JSQMessagesInputToolbarDelegate {
    func messagesInputToolbar(_ toolbar: JSQMessagesInputToolbar!, didPressRightBarButton sender: UIButton!) {
        self.goService.addPost(param: AddPostInputParam(latitude: self.coordinate.latitude, longitude: self.coordinate.longitude, message: toolbar.contentView.textView.text, typeId: self.talkType!), completion: { (result) in
            self.dismiss(animated: true, completion: nil)
        })

        toolbar.contentView.textView.text = ""
        toolbar.contentView.rightBarButtonItem.isEnabled = false
    }
    
    func messagesInputToolbar(_ toolbar: JSQMessagesInputToolbar!, didPressLeftBarButton sender: UIButton!) {
        
    }
}
