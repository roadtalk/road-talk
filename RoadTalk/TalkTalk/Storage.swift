//
//  Storage.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 28.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation


final class Storage {
    
    struct User: Codable {
        let facebookId:String
        let userId:String
    }
    
    private static let storage = UserDefaults.standard
    private static let userKey = "user"
    private static let facebookIdKey = "facebookIdKey"
    
    static func user() -> User? {
        return load(key: userKey)
    }
    
    static func facebookId() -> String? {
        return storage.object(forKey: facebookIdKey) as? String
    }
    
    static func save(user:User?) -> Void {
        save(obj: user, key: userKey)
    }
    
    static func save(facebookId: String) {
        storage.set(facebookId, forKey: facebookIdKey)
    }
    
    private static func save<T:Codable>(obj:T?, key:String) -> Void {
        if let o = obj {
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(o) {
                storage.set(encoded, forKey: key)
            }
        } else {
            storage.set(nil, forKey: key)
        }
        
        storage.synchronize()
    }
    
    private static func load<T:Codable>(key:String) -> T? {
        if let s = storage.object(forKey: key) as? Data {
            let decoder = JSONDecoder()
            if let l:T = try? decoder.decode(T.self, from: s) {
                return l
            }
        }
        return nil
    }
    
    
}
