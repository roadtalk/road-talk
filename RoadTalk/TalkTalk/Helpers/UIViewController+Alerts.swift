//
//  Alert.swift
//  TalkTalk
//
//  Created by Сергей Петрук on 28.07.2018.
//  Copyright © 2018 TalkTalk Team. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func showError(message:String, handler:((UIAlertAction)->())? = nil) ->Void {
        let alertController = UIAlertController(title: "Error", message:
            message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: handler))
        self.present(alertController, animated: true, completion: nil)
    }
}
